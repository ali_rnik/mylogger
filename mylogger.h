#ifndef MYLOGGER_H
#define MYLOGGER_H

#include <iostream>
#include <string>
#include <stdarg.h>
#include <fstream>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <time.h>

#define TOSTDOUT 1
#define TOFILE 1

class Mylogger
{
private:
  std::string file_path;
  std::ofstream myfile;
  time_t rawtime;
  tm *timeinfo;

public:
  Mylogger(std::string path);
  ~Mylogger();
  int toboth(char *format, ...);
  int tostdout(char *format, ...);
  int tofile(char *format, ...);

};

#endif
