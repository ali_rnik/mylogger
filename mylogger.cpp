#include "mylogger.h"

Mylogger::Mylogger(std::string path)
{
  file_path = path;
  myfile.close();
  myfile.open(file_path);
}

Mylogger::~Mylogger()
{
  myfile.close();
}
int Mylogger::tofile(char *format, ...)
{
  // check if logging disable
  if (TOFILE == 0)
    return 0;

  time(&rawtime);
  timeinfo = localtime(&rawtime);
  myfile << "### ";
  myfile << asctime(timeinfo);


  va_list ap;
  char *sval;
  int ival, i;
  double fval;

  va_start(ap, format);

  for (i = 0; i < (int) strlen(format); i++) {
    if (format[i] != '%') {
      myfile << format[i];
      continue;
    }
    i++;
    switch (format[i]) {
    case 's':
      for (sval = va_arg(ap, char*); *sval; sval++)
        myfile << *sval;
      break;
    case 'd':
      ival = va_arg(ap, int);
      myfile << std::to_string(ival);
      break;
    case 'f':
      fval = va_arg(ap, double);
      myfile << std::to_string(fval);
      break;
    default:
      break;
    }
  }
  va_end(ap);

  myfile.flush();
  return 0;
}

int Mylogger::tostdout(char *format, ...)
{
  // check if logging disable
  if (TOSTDOUT == 0)
    return 0;

  time(&rawtime);
  timeinfo = localtime(&rawtime);
  printf("### %s", asctime(timeinfo));

  setbuf(stdout, NULL);


  va_list ap;
  char *sval;
  int ival, i;
  double fval;

  va_start(ap, format);

  for (i = 0; i < (int) strlen(format); i++) {
    if (format[i] != '%') {
      putchar((int) format[i]);
      continue;
    }
    i++;
    switch (format[i]) {
    case 's':
      for (sval = va_arg(ap, char*); *sval; sval++)
        putchar((int) *sval);
      break;
    case 'd':
      ival = va_arg(ap, int);
      printf("%d", ival);
      break;
    case 'f':
      fval = va_arg(ap, double);
      printf("%f", fval);
      break;
    default:
      break;
    }
  }
  va_end(ap);

  myfile.flush();
  return 0;
}

int Mylogger::toboth(char *format, ...)
{
  // check if logging disable
  if (TOSTDOUT == 0 && TOFILE == 0)
    return 0;

  time(&rawtime);
  timeinfo = localtime(&rawtime);
  printf("### %s", asctime(timeinfo));
  myfile << "### ";
  myfile << asctime(timeinfo);

  setbuf(stdout, NULL);

  va_list ap;
  char *sval;
  int ival, i;
  double fval;

  va_start(ap, format);

  for (i = 0; i < (int) strlen(format); i++) {
    if (format[i] != '%') {
      printf("%c", format[i]);
      myfile << format[i];
      continue;
    }
    i++;
    switch (format[i]) {
    case 's':
      for (sval = va_arg(ap, char*); *sval; sval++) {
        printf("%c", *sval);
        myfile << *sval;
      }
      break;
    case 'd':
      ival = va_arg(ap, int);
      myfile << std::to_string(ival);
      printf("%d", ival);
      break;
    case 'f':
      fval = va_arg(ap, double);
      myfile << std::to_string(fval);
      printf("%f", fval);
      break;
    default:
      break;
    }
  }
  va_end(ap);

  myfile.flush();
  return 0;
}
